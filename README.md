![Build Status](https://gitlab.com/rwhogg/rwhogg.site/badges/master/build.svg)

---

My personal webpage (WIP)

---

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install] Nuxt
1. Generate and preview the website with hot-reloading: `npm run dev` or `nuxt`
1. Add content

Read more at Nuxt's [documentation].

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Also, please remove the entry containing the baseurl of your site on the file `nuxt.config.js`:

```javascript
router: {
    base: '/nuxt/'
},
```

Read more about [user/group Pages][userpages] and [project Pages][projpages].


[ci]: https://about.gitlab.com/gitlab-ci/
[Nuxt]: https://nuxtjs.org/
[install]: https://nuxtjs.org/guide/installation/
[documentation]: https://nuxtjs.org/guide
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages